import template from 'handlebars';
import fs from 'fs';
import yaml from 'node-yaml';

var f = fs.readFileSync('base.js', 'utf-8');
var base = template.compile(f, {noEscape: true});
var out = base(model());
console.log(out);

function model() {
  var model = {
    items: [],
  };

  var data = yaml.readSync('defs.yaml');
  
  for (var typeID in data.types) {
    var type = data.types[typeID];
    var source = "";

    for (var groupID in data.operations) {
      var group = data.operations[groupID];

      for (var opID in group.ops) {
        var op = group.ops[opID];
        var t = template.compile(group.template);

        source += t({
          key: opID,
          id: typeID,
          op: op.code,
          type: type,
        });
      }

      model.items.push({
        id: typeID,
        type: type,
        source: source,
        head: fs.readFileSync(typeID + '.js', 'utf-8'),
      });
    }
  }

  return model;
}
