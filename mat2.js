/**
 * Mat2 is a matrix for 2d affine transformations.
 * It can be created using:
 * 
 *     var mat = new la.Mat2(); // results in an identity matrix
 * 
 * I't can also be created withouth the new keyword:
 *
 *     var vec = la.Mat2();
 *
 * The components are arranged like:
 *
 *     +---+---+---+
 *     | a | c | e |
 *     +---+---+---+
 *     | b | d | f |
 *     +---+---+---+
 */
function Mat2(a, b, c, d, e, f) {
  if (!(this instanceof Mat2)) {
      return new Mat2(a, b, c, d, e, f);
  }

  if (typeof a === 'undefined') {
    this.a = this.d = 1;
    this.b = this.c = this.e = this.f = 0;
    return;
  }

  if (a instanceof Object) {
    this.a = a.a || 0;
    this.b = a.b || 0;
    this.c = a.c || 0;
    this.d = a.d || 0;
    this.e = a.e || 0;
    this.f = a.f || 0;
    return;
  }

  if (a instanceof Array) {
    if (a.length < 6) {
      throw "To construct a Mat2 from an array the array needs to be at least of length 6";
    }
    this.a = a[0];
    this.b = a[1];
    this.c = a[2];
    this.d = a[3];
    this.e = a[4];
    this.f = a[5];
    return;
  }

  this.a = a || 0;
  this.b = b || 0;
  this.c = c || 0;
  this.d = d || 0;
  this.e = e || 0;
  this.f = f || 0;
}

Mat2.prototype.string = function(){
  return "[" + this.a + "," + this.c + "," + this.e + "]\n" +
  "[" + this.b + "," + this.d + "," + this.f + "]";
};

Mat2.prototype.multiply = function(mat, to) {
  var a = this.a,
    b = this.b,
    c = this.c,
    d = this.d,
    e = this.e,
    f = this.f;

  if (typeof to === 'undefined') {
    to = this;
  }

  to.a = a * mat.a + c * mat.b;
  to.b = b * mat.a + d * mat.b;
  to.c = a * mat.c + c * mat.d;
  to.d = b * mat.c + d * mat.d;
  to.e = a * mat.e + c * mat.f + e;
  to.f = b * mat.e + d * mat.f + f;

  return to;
};

Mat2.prototype.translate = function(x, y, to) {
  return this.multiply(new Mat2(1, 0, 0, 1, x, y), to);
};

Mat2.prototype.scale = function(x, y, to) {
  return this.multiply(new Mat2(x, 0, 0, y, 0, 0), to);
};

Mat2.prototype.clone = function() {
  return new Mat2(this);
};

Mat2.prototype.inverse = function() {
  var	dt = this.determinant();
  if (Math.abs(dt) < 1e-14) {
    throw "Matrix not invertible.";
  }

  return new Mat2(
    this.d / dt,
    -this.b / dt,
    -this.c / dt,
    this.a / dt,
    (this.c * this.f - this.d * this.e) / dt,
    -(this.a * this.f - this.b * this.e) / dt
  );
};

Mat2.prototype.determinant = function() {
  return this.a * this.d - this.b * this.c;
};

Mat2.prototype.transformArray = function(points) {
  var result = [];
  var p, i;
  if (typeof points[0] === 'number') {
    for(i = 0; i < points.length; i += 2) {
      p = this.transform(points[i], points[i + 1]);
      result.push(p.x, p.y);
    }
  } else {
    for(i = 0; i < points.length; i++) {
      p = points[i];
      result.push(this.transform(p.x, p.y));
    }
  }
  return result;
};

Mat2.prototype.transform = function(x, y) {
  if (typeof x === 'number') {
    return new la.Vec2(
      x * this.a + y * this.c + this.e,
      x * this.b + y * this.d + this.f
    );
  }
  return new la.Vec2(
    x.x * this.a + x.y * this.c + this.e,
    x.x * this.b + x.y * this.d + this.f
  );
};


