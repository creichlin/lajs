// this code is generated from:
// - generate.mjs (script)
// - base.js (template)
// - defs.yaml
{{#items}}
// - {{id}}.js
{{/items}}
(function(){

  {{#items}}
{{ head }}

{{ source }}

  {{/items}}

  if(typeof module !== 'undefined') {
    module.exports = {
      {{#items}}
      {{type.idu}}: {{type.idu}},
      {{/items}}
    };
  } else {
    window.la = window.la || {};
    {{#items}}
    window.la.{{type.idu}} = {{type.idu}};
    {{/items}}
  }
})();
