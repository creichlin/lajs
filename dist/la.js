// this code is generated from:
// - generate.mjs (script)
// - base.js (template)
// - defs.yaml
// - vec2.js
// - vec3.js
// - mat2.js
(function() {


    /**
     * Vec2 is a vector in two dimensions with an x an y component.
     * It can be created using:
     * 
     *     var vec = new Vec2(); // results in a vector with components 0, 0
     * 
     * I't can also be created withouth the new keyword:
     * 
     *     var vec = Vec2(3, 4);
     */
    function Vec2(x, y) {
        if (!(this instanceof Vec2)) {
            return new Vec2(x, y);
        }

        // we have to check array before object beacuse object check will also trigger on arrays
        if (Array.isArray(x)) {
            if (x.length < 2) {
                throw "To construct a Vec2 from an array the array needs to be at least of length 2";
            }
            this.x = x[0];
            this.y = x[1];
            return;
        }

        if (x instanceof Object) {
            this.x = x.x || 0;
            this.y = x.y || 0;
            return;
        }

        this.x = x || 0;
        this.y = y || 0;
    }

    Vec2.prototype.rotateDeg = function(angle, to) {
        return this.rotate(angle * Math.PI / 180, to);
    };

    Vec2.prototype.angle = function() {
        return Math.atan2(this.y, this.x);
    };

    Vec2.prototype.rotate = function(angle, to) {
        if (typeof to == 'undefined') {
            to = this;
        }
        var tx = this.x * Math.cos(angle) - this.y * Math.sin(angle);
        to.y = this.x * Math.sin(angle) + this.y * Math.cos(angle);
        to.x = tx;
        return to;
    };

    Vec2.prototype.clone = function() {
        return new Vec2(this);
    };

    Vec2.prototype.normalize = function(to) {
        var length = this.length();

        if (length === 0) {
            if (typeof to == 'undefined') {
                to = this;
            }
            to.x = 1;
            to.y = 0;
            return to;
        }
        return this.div(new Vec2(length, length), to);
    };

    Vec2.prototype.distance = function(vec) {
        return this.sub(vec, Vec2()).length();

    };

    Vec2.prototype.length = function() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    };



    Vec2.prototype.mul = function(vec2, to) {
        if (to === undefined) {
            to = this;
        }
        to.x = this.x * vec2.x;
        to.y = this.y * vec2.y;
        return to;
    };

    Vec2.prototype.div = function(vec2, to) {
        if (to === undefined) {
            to = this;
        }
        to.x = this.x / vec2.x;
        to.y = this.y / vec2.y;
        return to;
    };

    Vec2.prototype.sub = function(vec2, to) {
        if (to === undefined) {
            to = this;
        }
        to.x = this.x - vec2.x;
        to.y = this.y - vec2.y;
        return to;
    };

    Vec2.prototype.add = function(vec2, to) {
        if (to === undefined) {
            to = this;
        }
        to.x = this.x + vec2.x;
        to.y = this.y + vec2.y;
        return to;
    };

    Vec2.prototype.mod = function(vec2, to) {
        if (to === undefined) {
            to = this;
        }
        to.x = this.x % vec2.x;
        to.y = this.y % vec2.y;
        return to;
    };


    function Vec3(x, y, z) {
        if (!(this instanceof Vec3)) {
            return new Vec3(x, y, z);
        }

        // we have to check array before object beacuse object check will also trigger on arrays
        if (Array.isArray(x)) {
            if (x.length < 3) {
                throw "To construct a Vec3 from an array the array needs to be at least of length 3";
            }
            this.x = x[0];
            this.y = x[1];
            this.z = x[2];
            return;
        }

        if (x instanceof Object) {
            this.x = x.x || 0;
            this.y = x.y || 0;
            this.z = x.z || 0;
            return;
        }

        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    }


    Vec3.prototype.mul = function(vec3, to) {
        if (to === undefined) {
            to = this;
        }
        to.x = this.x * vec3.x;
        to.y = this.y * vec3.y;
        to.z = this.z * vec3.z;
        return to;
    };

    Vec3.prototype.div = function(vec3, to) {
        if (to === undefined) {
            to = this;
        }
        to.x = this.x / vec3.x;
        to.y = this.y / vec3.y;
        to.z = this.z / vec3.z;
        return to;
    };

    Vec3.prototype.sub = function(vec3, to) {
        if (to === undefined) {
            to = this;
        }
        to.x = this.x - vec3.x;
        to.y = this.y - vec3.y;
        to.z = this.z - vec3.z;
        return to;
    };

    Vec3.prototype.add = function(vec3, to) {
        if (to === undefined) {
            to = this;
        }
        to.x = this.x + vec3.x;
        to.y = this.y + vec3.y;
        to.z = this.z + vec3.z;
        return to;
    };

    Vec3.prototype.mod = function(vec3, to) {
        if (to === undefined) {
            to = this;
        }
        to.x = this.x % vec3.x;
        to.y = this.y % vec3.y;
        to.z = this.z % vec3.z;
        return to;
    };


    /**
     * Mat2 is a matrix for 2d affine transformations.
     * It can be created using:
     * 
     *     var mat = new la.Mat2(); // results in an identity matrix
     * 
     * I't can also be created withouth the new keyword:
     *
     *     var vec = la.Mat2();
     *
     * The components are arranged like:
     *
     *     +---+---+---+
     *     | a | c | e |
     *     +---+---+---+
     *     | b | d | f |
     *     +---+---+---+
     */
    function Mat2(a, b, c, d, e, f) {
        if (!(this instanceof Mat2)) {
            return new Mat2(a, b, c, d, e, f);
        }

        if (typeof a === 'undefined') {
            this.a = this.d = 1;
            this.b = this.c = this.e = this.f = 0;
            return;
        }

        if (a instanceof Object) {
            this.a = a.a || 0;
            this.b = a.b || 0;
            this.c = a.c || 0;
            this.d = a.d || 0;
            this.e = a.e || 0;
            this.f = a.f || 0;
            return;
        }

        if (a instanceof Array) {
            if (a.length < 6) {
                throw "To construct a Mat2 from an array the array needs to be at least of length 6";
            }
            this.a = a[0];
            this.b = a[1];
            this.c = a[2];
            this.d = a[3];
            this.e = a[4];
            this.f = a[5];
            return;
        }

        this.a = a || 0;
        this.b = b || 0;
        this.c = c || 0;
        this.d = d || 0;
        this.e = e || 0;
        this.f = f || 0;
    }

    Mat2.prototype.string = function() {
        return "[" + this.a + "," + this.c + "," + this.e + "]\n" +
            "[" + this.b + "," + this.d + "," + this.f + "]";
    };

    Mat2.prototype.multiply = function(mat, to) {
        var a = this.a,
            b = this.b,
            c = this.c,
            d = this.d,
            e = this.e,
            f = this.f;

        if (typeof to === 'undefined') {
            to = this;
        }

        to.a = a * mat.a + c * mat.b;
        to.b = b * mat.a + d * mat.b;
        to.c = a * mat.c + c * mat.d;
        to.d = b * mat.c + d * mat.d;
        to.e = a * mat.e + c * mat.f + e;
        to.f = b * mat.e + d * mat.f + f;

        return to;
    };

    Mat2.prototype.translate = function(x, y, to) {
        return this.multiply(new Mat2(1, 0, 0, 1, x, y), to);
    };

    Mat2.prototype.scale = function(x, y, to) {
        return this.multiply(new Mat2(x, 0, 0, y, 0, 0), to);
    };

    Mat2.prototype.clone = function() {
        return new Mat2(this);
    };

    Mat2.prototype.inverse = function() {
        var dt = this.determinant();
        if (Math.abs(dt) < 1e-14) {
            throw "Matrix not invertible.";
        }

        return new Mat2(
            this.d / dt, -this.b / dt, -this.c / dt,
            this.a / dt,
            (this.c * this.f - this.d * this.e) / dt, -(this.a * this.f - this.b * this.e) / dt
        );
    };

    Mat2.prototype.determinant = function() {
        return this.a * this.d - this.b * this.c;
    };

    Mat2.prototype.transformArray = function(points) {
        var result = [];
        var p, i;
        if (typeof points[0] === 'number') {
            for (i = 0; i < points.length; i += 2) {
                p = this.transform(points[i], points[i + 1]);
                result.push(p.x, p.y);
            }
        } else {
            for (i = 0; i < points.length; i++) {
                p = points[i];
                result.push(this.transform(p.x, p.y));
            }
        }
        return result;
    };

    Mat2.prototype.transform = function(x, y) {
        if (typeof x === 'number') {
            return new la.Vec2(
                x * this.a + y * this.c + this.e,
                x * this.b + y * this.d + this.f
            );
        }
        return new la.Vec2(
            x.x * this.a + x.y * this.c + this.e,
            x.x * this.b + x.y * this.d + this.f
        );
    };





    Mat2.prototype.mul = function(mat2, to) {
        if (to === undefined) {
            to = this;
        }
        to.a = this.a * mat2.a;
        to.b = this.b * mat2.b;
        to.c = this.c * mat2.c;
        to.d = this.d * mat2.d;
        to.e = this.e * mat2.e;
        to.f = this.f * mat2.f;
        return to;
    };

    Mat2.prototype.div = function(mat2, to) {
        if (to === undefined) {
            to = this;
        }
        to.a = this.a / mat2.a;
        to.b = this.b / mat2.b;
        to.c = this.c / mat2.c;
        to.d = this.d / mat2.d;
        to.e = this.e / mat2.e;
        to.f = this.f / mat2.f;
        return to;
    };

    Mat2.prototype.sub = function(mat2, to) {
        if (to === undefined) {
            to = this;
        }
        to.a = this.a - mat2.a;
        to.b = this.b - mat2.b;
        to.c = this.c - mat2.c;
        to.d = this.d - mat2.d;
        to.e = this.e - mat2.e;
        to.f = this.f - mat2.f;
        return to;
    };

    Mat2.prototype.add = function(mat2, to) {
        if (to === undefined) {
            to = this;
        }
        to.a = this.a + mat2.a;
        to.b = this.b + mat2.b;
        to.c = this.c + mat2.c;
        to.d = this.d + mat2.d;
        to.e = this.e + mat2.e;
        to.f = this.f + mat2.f;
        return to;
    };

    Mat2.prototype.mod = function(mat2, to) {
        if (to === undefined) {
            to = this;
        }
        to.a = this.a % mat2.a;
        to.b = this.b % mat2.b;
        to.c = this.c % mat2.c;
        to.d = this.d % mat2.d;
        to.e = this.e % mat2.e;
        to.f = this.f % mat2.f;
        return to;
    };



    if (typeof module !== 'undefined') {
        module.exports = {
            Vec2: Vec2,
            Vec3: Vec3,
            Mat2: Mat2,
        };
    } else {
        window.la = window.la || {};
        window.la.Vec2 = Vec2;
        window.la.Vec3 = Vec3;
        window.la.Mat2 = Mat2;
    }
})();