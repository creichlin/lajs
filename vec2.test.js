import la from './dist/la.js';
import test from 'ava';

test('default constructor', t => {
  var v = la.Vec2();
  t.is(v.x, 0);
  t.is(v.y, 0);
});

test('one number constructor', t => {
  var v = la.Vec2(7);
  t.is(v.x, 7);
  t.is(v.y, 0);
});

test('two numbers constructor', t => {
  var v = la.Vec2(1, 4);
  t.is(v.x, 1);
  t.is(v.y, 4);
});

test('array constructor', t => {
  var v = la.Vec2([1, 4]);
  t.is(v.x, 1);
  t.is(v.y, 4);
});

test('object constructor', t => {
  var v = la.Vec2({x: 1, y:4});
  t.is(v.x, 1);
  t.is(v.y, 4);
});

test('add', t => {
  var v = la.Vec2(1, 3);
  var n = v.add(la.Vec2(1, 2));
  t.deepEqual(v, la.Vec2(2, 5));
  t.deepEqual(n, la.Vec2(2, 5));
});

test('add to', t => {
  var v = la.Vec2(1, 3);
  var n = v.add(la.Vec2(1, 2), la.Vec2());
  t.deepEqual(v, la.Vec2(1, 3));
  t.deepEqual(n, la.Vec2(2, 5));
});

test('sub', t => {
  var v = la.Vec2(1, 3);
  var n = v.sub(la.Vec2(1, 2));
  t.deepEqual(v, la.Vec2(0, 1));
  t.deepEqual(n, la.Vec2(0, 1));
});

test('sub to', t => {
  var v = la.Vec2(1, 3);
  var n = v.sub(la.Vec2(1, 2), la.Vec2());
  t.deepEqual(v, la.Vec2(1, 3));
  t.deepEqual(n, la.Vec2(0, 1));
});

test('mul', t => {
  var v = la.Vec2(1, 3);
  var n = v.mul(la.Vec2(1, 2));
  t.deepEqual(v, la.Vec2(1, 6));
  t.deepEqual(n, la.Vec2(1, 6));
});

test('mul to', t => {
  var v = la.Vec2(1, 3);
  var n = v.mul(la.Vec2(1, 2), la.Vec2());
  t.deepEqual(v, la.Vec2(1, 3));
  t.deepEqual(n, la.Vec2(1, 6));
});

test('div', t => {
  var v = la.Vec2(4, 12);
  var n = v.div(la.Vec2(2, 3));
  t.deepEqual(v, la.Vec2(2, 4));
  t.deepEqual(n, la.Vec2(2, 4));
});

test('div to', t => {
  var v = la.Vec2(4, 12);
  var n = v.div(la.Vec2(2, 3), la.Vec2());
  t.deepEqual(v, la.Vec2(4, 12));
  t.deepEqual(n, la.Vec2(2, 4));
});

test('rotate', t => {
  var v = la.Vec2(1, 0).rotate(Math.PI / 4);
  t.deepEqual(v, la.Vec2(0.7071067811865476, 0.7071067811865475));
});

test('distance', t => {
  var d = la.Vec2().distance(la.Vec2(0, 7));
  t.is(d, 7);
});

test('distance diagonal', t => {
  var d = la.Vec2(3, 4).distance(la.Vec2(0, 0));
  t.is(d, 5);
});