
/**
 * Vec2 is a vector in two dimensions with an x an y component.
 * It can be created using:
 * 
 *     var vec = new Vec2(); // results in a vector with components 0, 0
 * 
 * I't can also be created withouth the new keyword:
 * 
 *     var vec = Vec2(3, 4);
 */
function Vec2(x, y) {
  if (!(this instanceof Vec2)) {
      return new Vec2(x, y);
  }

  // we have to check array before object beacuse object check will also trigger on arrays
  if (Array.isArray(x)) {
    if (x.length < 2) {
      throw "To construct a Vec2 from an array the array needs to be at least of length 2";
    }
    this.x = x[0];
    this.y = x[1];
    return;
  }

  if (x instanceof Object) {
    this.x = x.x || 0;
    this.y = x.y || 0;
    return;
  }

  this.x = x || 0;
  this.y = y || 0;
}

Vec2.prototype.rotateDeg = function (angle, to) {
  return this.rotate(angle * Math.PI / 180, to);
};

Vec2.prototype.angle = function () {
  return Math.atan2(this.y, this.x);
};  

Vec2.prototype.rotate = function (angle, to) {
  if(typeof to == 'undefined') {
    to = this;
  }
  var tx = this.x * Math.cos(angle) - this.y * Math.sin(angle);
  to.y = this.x * Math.sin(angle) + this.y * Math.cos(angle);
  to.x = tx;
  return to;
};  

Vec2.prototype.clone = function() {
  return new Vec2(this);
};

Vec2.prototype.normalize = function(to) {
  var length = this.length();

  if (length === 0) {
      if(typeof to == 'undefined') {
        to = this;
      }
      to.x = 1;
      to.y = 0;
      return to;
  }
  return this.div(new Vec2(length, length), to);
};

Vec2.prototype.distance = function(vec) {
  return this.sub(vec, Vec2()).length();
  
};

Vec2.prototype.length = function () {
  return Math.sqrt(this.x * this.x + this.y * this.y);
};
