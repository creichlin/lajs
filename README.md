lajs
====

Linear algebra lib for calculations in 2 and 3 dimensions.

Intended usage is for computer graphics.

*Work in progress*

Installation
============

    npm install lajs

Examples
========

    var la = require('lajs');
    var vec = la.Vec2(4, 1);
    vec.add(la.Vec(1, 2));

    var other = vec.add(la.Vec(0.5, 0.5));

Types
=====

Vec2
----

Vector in two dimensions with components x and y.

Vec3
----

Vector in three dimensions with components x, y and z.

Mat2
----

Matrix with two columns and three rows for addine transformations in two dimensions.

