function Vec3(x, y, z) {
  if (!(this instanceof Vec3)) {
      return new Vec3(x, y, z);
  }

  // we have to check array before object beacuse object check will also trigger on arrays
  if (Array.isArray(x)) {
    if (x.length < 3) {
      throw "To construct a Vec3 from an array the array needs to be at least of length 3";
    }
    this.x = x[0];
    this.y = x[1];
    this.z = x[2];
    return;
  }

  if (x instanceof Object) {
    this.x = x.x || 0;
    this.y = x.y || 0;
    this.z = x.z || 0;
    return;
  }

  this.x = x || 0;
  this.y = y || 0;
  this.z = z || 0;
}