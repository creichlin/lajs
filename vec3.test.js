import la from './dist/la.js';
import test from 'ava';

function isVec3(t, v, x, y, z) {
  t.is(v.x, x);
  t.is(v.y, y);
  t.is(v.z, z);
}

test('default constructor', t => {
  var v = la.Vec3();
  isVec3(t, v, 0, 0, 0);
});

test('one number constructor', t => {
  var v = la.Vec3(7);
  isVec3(t, v, 7, 0, 0);
});

test('two numbers constructor', t => {
  var v = la.Vec3(1, 4, 5);
  isVec3(t, v, 1, 4, 5);
});

test('array constructor', t => {
  var v = la.Vec3([1, 4, 5]);
  isVec3(t, v, 1, 4, 5);
});

test('object constructor', t => {
  var v = la.Vec3({x: 1, y:4, z: 5});
  isVec3(t, v, 1, 4, 5);
});

